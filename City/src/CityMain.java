import java.util.Scanner;                           //Import the scanner

public class CityMain {                             //Main class
	public static void main (String args []){       //Main method
		Scanner input = new Scanner(System.in);     //Initialize scanner with the name input

		int citizens = 6;                           //Starting amounts
		int cropfields = 0;
		int houses = 3 ;
        int food = 20;
        int gold = 100;
        int happiness = 100;

        final String[] taxrateOptions = new String[4];

        taxrateOptions[0] = "None";
        taxrateOptions[1] = "Low";
        taxrateOptions[2] = "Medium";
        taxrateOptions[3] = "High";

        String taxrate = "None";
        String enter;
        String cityname;

		System.out.println("You, and 5 other people have been set out to create another colony.");  //Storyline
		System.out.println("Make choices that can either ruin or save your city.");
		System.out.println("Good luck!");
        System.out.println("");

        System.out.println("Press enter to start your journey.");
		
		enter = input.nextLine();
		
		if (enter.equals("")){
			System.out.println("You have arrived to a perfect piece of land to start your city.");  //Storyline
			System.out.println("How would you like to name your city?");

            cityname = input.nextLine();

            System.out.println("");
			System.out.println("Your city's name is: " + cityname );        //Save cityname
            System.out.println("");

            System.out.println("Press enter to continue.");
			
			enter = input.nextLine();

            if (enter.equals("")) {
                System.out.println("In this game you need to make crop fields");    //Daily demands
                System.out.println("to fulfill the growing demand of food.");
                System.out.println("Every 1 cropfield produces 3 food per turn.");
                System.out.println("For every 3 citizens 1 food is decreased per day.");
                System.out.println("Building a cropfield costs 30 Gold");
                System.out.println("");
                System.out.println("You also need to build houses to let the population grow.");
                System.out.println("For every 3 citizens, you need to build 1 house.");
                System.out.println("Building a house costs 50 gold.");
                System.out.println("Whenever you not fulfill one of these demands your");
                System.out.println("population will shrink by 3 citizens every turn.");
                System.out.println("You can choose to see these rules again at the Status Report.");
                System.out.println("");

                System.out.println("Press enter to continue.");
            }
			enter = input.nextLine();
			
			if (enter.equals("")){                                              //Going to the CityStatus class
			    CityStatus.status(food, citizens, happiness, cropfields, houses, gold, taxrate, taxrateOptions);    //Sending all the variables
            }
		}
	}
}



class CityStatus {
	public static void status (int food, int citizens, int happiness, int cropfields, int houses, int gold, String taxrate, String[] taxrateOptions){ //Getting all the variables
		Scanner input = new Scanner(System.in);         //Initialize the scanner
		
		String enter;
        String seeRules;

        int happinessStarvation = 10;

        if(food > 0) {                            //This is the daily food consumption
            int fooddecreasing = citizens / 3;    //Every 3 citizens use 1 food per day
            food = food - fooddecreasing;
        }

        if(food <= 0){                            //If the food is lower than or is 0 a citizen dies
            citizens--;
            happiness = happiness - happinessStarvation;
            System.out.println("A citizen has died of starvation!");

        }
        else if(citizens / 3 < houses){           //If there is enough food and enough houses
            citizens++;                           //A citizen is born
            System.out.println("A new citizen has been born!");
        }

        if(cropfields>0){                         //Here the outcome of the cropfields
            int foodincreasing = cropfields * 3;  //is added to the food, 3 food per cropfield
            food = food + foodincreasing;
        }


        if (taxrate.equalsIgnoreCase("Low")){

        }

        if (taxrate.equalsIgnoreCase("Medium")){

        }

        if (taxrate.equalsIgnoreCase("High")){

        }

        System.out.println("Status report:");     //This is the daily status report.
		System.out.println("You have:");
		System.out.println(gold + " gold");
        System.out.println(citizens + " citizen(s)");
		System.out.println(houses + " house(s)");
		System.out.println(food + " food");
		System.out.println(cropfields + " cropfield(s)");
        System.out.println("");
        System.out.println("Your happiness is at: " + happiness + "%");
        System.out.println("");
        System.out.println("Your taxrate is: " + taxrate);
        System.out.println("");

		System.out.println("Press enter to continue.");

		enter = input.nextLine();
        if (enter.equals("")) {
            System.out.println("Do you want to see the rules again? Yes/No");  //Asks the player if he wants to see the rules again
            seeRules = input.nextLine();                                //If yes, show the rules, if no, continue to CityRoutine
            if(seeRules.equalsIgnoreCase("Yes")){
                System.out.println("");
                System.out.println("Every 1 cropfield produces 3 food per turn.");
                System.out.println("For every 3 citizens 1 food is decreased per day.");
                System.out.println("Building a cropfield costs 30 Gold");
                System.out.println("");
                System.out.println("You also need to build houses to let the population grow.");
                System.out.println("For every 3 citizens, you need to build 1 house.");
                System.out.println("Building a house costs 50 gold.");
                System.out.println("Whenever you not fulfill one of these demands your");
                System.out.println("population will shrink by 3 citizens every turn.");
                System.out.println("You can choose to see these rules again at the Status Report.");
                System.out.println("");
            }
            if(seeRules.equalsIgnoreCase("No")){
                CityRoutine.routine(food,citizens, happiness, cropfields, houses, gold, taxrate, taxrateOptions);
            }

            System.out.println("Press enter to continue");

            enter = input.nextLine();

            if (enter.equals("")){
                CityRoutine.routine(food, citizens, happiness, cropfields, houses, gold, taxrate, taxrateOptions);
            }
		}
	}
}

class CityRoutine {
    public static void routine(int food, int citizens, int happiness, int cropfields, int houses, int gold, String taxrate, String[] taxrateOptions){ //Getting al the variables
        Scanner input = new Scanner(System.in); //Initializing the scanner

        String enter;
        String buildHouse;
        String buildCropField;
        String increaseTaxrate;

        int costHouse;
        costHouse = 50;

        int costCropfield;
        costCropfield = 30;

        System.out.println("You have " + gold + " gold");
        System.out.println("Your taxrate is: " + taxrate);
        System.out.println("");
        System.out.println("Do you want to increase the taxrate? Yes/No");

        increaseTaxrate = input.nextLine();

        System.out.println("");

        if (increaseTaxrate.equalsIgnoreCase("Yes")) {
            System.out.println("You can change the taxrate to:");
            System.out.println("");
            System.out.println(taxrateOptions[0]);
            System.out.println(taxrateOptions[1]);
            System.out.println(taxrateOptions[2]);
            System.out.println(taxrateOptions[3]);
            System.out.println("");
            System.out.println("What would you like it to be?");
            System.out.println("");
            taxrate = input.nextLine();
        }


        if(increaseTaxrate.equalsIgnoreCase("No")) {
            System.out.println("");
            System.out.println("You haven't increased the taxrate.");
            System.out.println("");
        }

        System.out.println("Press enter to continue");

        enter = input.nextLine();

        if (enter.equals("")) {
            System.out.println("");
        }
        System.out.println("Do you want to build a new house?");    //Asks if the player wants to build a new house
        System.out.println("Building a house costs 50 gold.");
        System.out.println("You have " + gold + " gold");
        System.out.println("");
        System.out.println("Yes/No");                               //If yes, a house is added, if no, nothing happens

        buildHouse = input.nextLine();

        if (!buildHouse.equalsIgnoreCase("Yes") || !buildHouse.equalsIgnoreCase("No")) {
            System.out.println("You entered in something wrong.");

            System.out.println("");

            System.out.println("Do you want to build a new house?");
            System.out.println("Building a house costs 50 gold.");
            System.out.println("You have " + gold + " gold");
            System.out.println("");
            System.out.println("Yes/No");

            buildHouse = input.nextLine();

        }

        if (buildHouse.equalsIgnoreCase("Yes")) {
            if(gold < costHouse){
                System.out.println("You don't have enough money to build a house.");
            }
            if(gold >= costHouse) {
                houses++;
                gold = gold - costHouse;
                System.out.println("");
                System.out.println("You have built a new house!");
                System.out.println("");
            }

        }
        if(buildHouse.equalsIgnoreCase("No")) {
            System.out.println("");
            System.out.println("You haven't built a new house.");
            System.out.println("");
        }

        System.out.println("Press enter to continue");

        enter = input.nextLine();

        if(enter.equals("")){                                            //Asks if the player wants to build a new cropfield
            System.out.println("Do you want to build a new cropfield?"); //If yes, a  cropfield is added
            System.out.println("Building a cropfield costs 30 gold.");
            System.out.println("You have " + gold + " gold");
            System.out.println("");
            System.out.println("Yes/No");                                //if no, nothing happens
        }

        buildCropField = input.nextLine();

        if (!buildCropField.equalsIgnoreCase("Yes") || !buildCropField.equalsIgnoreCase("No")) {
            System.out.println("You entered in something wrong.");

            System.out.println("");

            System.out.println("Do you want to build a new cropfield?");
            System.out.println("Building a house costs 30 gold.");
            System.out.println("You have " + gold + " gold");
            System.out.println("");
            System.out.println("Yes/No");

            buildCropField = input.nextLine();

        }

        if(buildCropField.equalsIgnoreCase("Yes")) {
            if (gold < costCropfield) {
                System.out.println("You don't have enough money to build a cropfield.");
            }
            if (gold >= costCropfield) {
                cropfields++;
                gold = gold - costCropfield;
                System.out.println("");
                System.out.println("You have built a new cropfield!");
                System.out.println("");
            }
        }
        if(buildCropField.equalsIgnoreCase("No")) {
            System.out.println("");
            System.out.println("You haven't built a new cropfield.");
            System.out.println("");
        }

        System.out.println("Press enter to continue");

        enter = input.nextLine();

        if(enter.equals("")) {  //Going back to the status class
            CityStatus.status(food, citizens, happiness, cropfields, houses, gold, taxrate, taxrateOptions);    //Sending all the variables
        }
    }
}

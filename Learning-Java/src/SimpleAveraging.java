import java.util.Scanner;

public class SimpleAveraging {
	public static void main (String [] args){
		Scanner input = new Scanner(System.in);
		
		double total = 0;
		double number;
		int counter = 0;
		double average;
		int AmountOfNumbers;
		
		System.out.println("How many numbers do you want to enter?");
		AmountOfNumbers = input.nextInt();
		
		while (counter < AmountOfNumbers){
			number = input.nextDouble();
			total = total + number;
			counter++;
		}
		average = total/AmountOfNumbers;
		System.out.println("The average is: " + average);
	}
}

public class HelloWorld { 					  //Elk Java program bestaat uit classes, verschillende stukjes code. 
						                      //Public betekend dat deze class beschikbaar is voor andere classes. Deze class heet HelloWorld.
	public static void main (String [] args){ //Dit is de "Method Header". De computer zoekt eerst de method met de naam: "main". 
		                                      //Is die er niet? Dan weet de computer niet wat hij moet doen.
		System.out.println("Hello World");    //Dit is de "Method Body". Hier typt hij een stukje tekst "out" naar de console.   
	}
}

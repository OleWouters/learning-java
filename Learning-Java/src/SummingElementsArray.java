public class SummingElementsArray {
    public static void main (String [] args){
        int number[]={21,86,43,32,78,31,92,86,10};
        int sum=0;

        for (int counter=0;counter<number.length;counter++){
            sum+=number[counter];
        }

        System.out.println("The sum of these numbers is: " +sum);
        System.out.println("The average of these numbers is: " +sum/number.length);
    }
}

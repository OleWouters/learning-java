public class ArraysWithMethods {
    public static void main (String [] args){
        int numbers[]={4,7,3,1,0,8};
        change(numbers);

        for(int y:numbers) {
            System.out.println("Using enhanced for");
            System.out.println(y);
        }
        for(int counter=0;counter<numbers.length;counter++) {
            System.out.println("Using normal for");
            System.out.println(numbers[counter]);
        }
    }
    public static void change(int x[]){
        for(int counter=0;counter<x.length;counter++)
            x[counter] += 5;
    }

}

import java.util.Scanner; 								//Hier importeer je de Scanner van java. 
														//Die registreert wat de gebruiker intypt.
public class Input {									//Public class met de naam "Input".
	public static void main (String [] args){			//Method header.
		Scanner Input = new Scanner(System.in);			//Hier wordt de variabel "Input" gelijk aan wat de gebruiker intypt.
		
		System.out.println("Type iets in");				//Hier schrijft het programma in de console: "Type iets in".
			
		System.out.println(Input.nextLine());			//Hier schrijft het programma de variabel "Input" in de console.
		

	
	}													//Dus wat de gebruiker intypt, wordt herhaald.
}

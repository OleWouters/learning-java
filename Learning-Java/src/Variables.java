public class Variables {						//Deze class is public en heet: "Variabelen".
	public static void main (String [] args){   //Deze method heet "main" en is public.
		double Nummer;							//Hier maak je een variabel genaamd "Nummer". Double betekend dat het
												//een nummer is met een decimaal erachter, bijvoorbeeld 5.28.
		Nummer = 5.28;							//Hier vertel je het programma dat de variabel Nummer gelijk is aan 5.28.
	
		System.out.println(Nummer);				//Hier schrijft het programma de variabe "Nummer" in de console.
	
	}
}

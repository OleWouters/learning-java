public class MultidimensionalArray {
    public static void main(String[] args){
        int firstarray [][]={{6,8,2,8,4,5},{8,2,7,5,1,6}};
        int secondarray [][]={{6,3,9,3,9,3},{76},{9,2,5,6,7,8}};

        System.out.println("This is the first array");
        display(firstarray);

        System.out.println("This is the second array");
        display(secondarray);
    }
    public static void display(int x [][]){
        for(int row=0;row<x.length;row++){
            for(int column=0;column<x[row].length;column++){
                System.out.print(x[row][column] +"\t");
            }
            System.out.println();
        }
    }
}

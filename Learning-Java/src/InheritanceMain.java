public class InheritanceMain {
    public static void main(String [] args){
        Inheritance2 Inheritance2Object = new Inheritance2();
        Inheritance3 Inheritance3Object = new Inheritance3();

        Inheritance2Object.fly();
        Inheritance3Object.fly();
    }
}

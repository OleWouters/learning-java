public class ConditionalOperator {
	public static void main(String [] args){
		int age = 32;
		
		System.out.println(age > 50 ? "You are old" : "You are young");
	}
}

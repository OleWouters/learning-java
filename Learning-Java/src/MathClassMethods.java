public class MathClassMethods {
    public static void main (String [] args){
        System.out.println(Math.pow(3,5));       //De vijfde macht van 3.
        System.out.println(Math.abs(-26.7));     //Hoe ver is het getal weg van 0.
        System.out.println(Math.floor(7.8));     //Rond het af naar beneden.
        System.out.println(Math.ceil(7.4));      //Rond het af naar boven.
        System.out.println(Math.max(8.6,6.7));   //Vertelt het hoogste nummer.
        System.out.println(Math.min(5.4,35));    //Vertel het laagste nummber.
        System.out.println(Math.sqrt(2));        //Vertelt de wortel van het nummer.
     }
}

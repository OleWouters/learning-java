package calculator;
import java.util.Scanner;

public class Calculator {
	public static void main (String [] args){
		Scanner scanner = new Scanner(System.in);
		
		String optie;
		
		CalculatorPlus plusObject = new CalculatorPlus ();
		
		CalculatorMultiply multiplyObject = new CalculatorMultiply ();
		
		CalculatorDivide divideObject = new CalculatorDivide ();
		
		CalculatorSubtract subtractObject = new CalculatorSubtract ();
		
		System.out.println("Wat wil je doen?");
		System.out.println("Optellen, aftrekken, vermenigvuldigen of delen?");
	
		optie = scanner.nextLine();
	
		if (optie.equals("optellen")){
			plusObject.plus();
		}
		if (optie.equals("Optellen")){
			plusObject.plus();
		}
		if (optie.equals("vermenigvuldigen")){
			multiplyObject.multiply();
		}
		if (optie.equals("Vermenigvuldigen"){
            multiplyObject.multiply();
        }
        if (optie.equals("delen")){
			divideObject.divide();
		}
		if (optie.equals("Delen")){
			divideObject.divide();
		}
		if (optie.equals("aftrekken")){
			subtractObject.subtract();
		}
		if (optie.equals("Aftrekken")){
			subtractObject.subtract();
		}
	}
}
